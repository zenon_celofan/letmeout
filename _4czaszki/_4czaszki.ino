#define DARK_THRESHOLD  300
#define HYSTERESIS  10

const int analog_pin[4] = {A0, A1, A2, A3};
const int led_pin[4] = {3, 4, 5, 6};
const int relay_pin = 2;

int sensor_value[4];
int trigger[4];

void setup() {
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(relay_pin, OUTPUT);

  for (int i = 0; i < 4; i++) {
    pinMode(led_pin[i], OUTPUT);
    digitalWrite(led_pin[i], HIGH);
  }

}

void loop() {
  int sum;

  for (int i = 0; i < 4; i++) {
    sensor_value[i] = analogRead(analog_pin[i]);
    if (sensor_value[i] < DARK_THRESHOLD - HYSTERESIS) {
      digitalWrite(LED_BUILTIN, LOW);
      trigger[i] = 1;
    } else if (sensor_value[i] > DARK_THRESHOLD + HYSTERESIS){
      trigger[i] = 0;
      digitalWrite(LED_BUILTIN, HIGH);
    }
  }
  
  sum = 0;
  for (int i = 0; i < 4; i ++) {
    sum = sum +trigger[i];
  }

  Serial.print("Threshold = ");
  Serial.println(DARK_THRESHOLD);
  Serial.print("Hysteresis = ");
  Serial.println(HYSTERESIS);
  
  for (int i = 0; i < 4; i++) {
    Serial.print("Sensor ");
    Serial.print(i);
    Serial.print(" = ");
    Serial.print(sensor_value[i]);
    Serial.print(" (");
    Serial.print(trigger[i]);
    Serial.print(")\n");
  }
  Serial.println();

  
  if (sum > 0) {
    digitalWrite(relay_pin, HIGH);
  } else {
    digitalWrite(relay_pin, LOW);
    delay(1000);
  }

  delay(100);
}
